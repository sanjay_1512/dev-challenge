import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from '../models/Customer';

const baseURL = 'https://customerdemoapi.herokuapp.com/api/customer/';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient: HttpClient) { }

  getCustomers(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(baseURL)
  }

  getCustomerById(id: string): Observable<Customer> {
    return this.httpClient.get<Customer>(`${baseURL}${id}`);
  }

  addNewCustomer(data: Customer): Observable<Customer> {
    return this.httpClient.post<Customer>(baseURL, data);
  }
}
