import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { CustomerService } from 'src/app/services/customer.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss'],
})
export class CreateCustomerComponent implements OnInit {
  formGroup!: FormGroup;
  required = 'This field is required';
  submitted = false;
  loading = false;

  constructor(
    private customerService: CustomerService,
    private formBuilder: FormBuilder,
    private router: Router,
    public snackBarService: ToastService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    const emailregex: RegExp =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const phoneRegex = /^!*([0-9]!*){10,}$/;
    this.formGroup = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern(emailregex)]],
      name: [null, Validators.required],
      address: [null, [Validators.required]],
      phone: [null, [Validators.required, Validators.pattern(phoneRegex)]],
    });
  }

  onSubmit(formValues: any) {
    this.loading = true;
    this.customerService.addNewCustomer(formValues).subscribe(
      (response) => {
        this.loading = false;
        this.submitted = true;
        this.snackBarService.openSnackBar('Customer successfully added!')
        this.router.navigateByUrl('/customers');
      },
      (error) => {
        this.loading = false;
        const {email, phone} = error.error
        if(email) {
          this.snackBarService.openSnackBar(email[0])
          return
        }
        if(phone) {
          this.snackBarService.openSnackBar(phone[0])
          return
        }
      }
    );
  }

  backToCustomerList() {
    this.router.navigateByUrl('/customers')
  }
}
