import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from 'src/app/models/Customer';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss'],
})
export class CustomerListComponent implements OnInit {
  customers: Customer[] = [];
  displayedColumns: string[] = ['name', 'email', 'phone', 'address'];
  loading = false;
  constructor(public customerService: CustomerService, private router: Router) {}

  ngOnInit(): void {
    this.getCustomers()
  }

  getCustomers(): void {
    this.loading = true
    this.customerService.getCustomers().subscribe(
      (customers) => {
        this.loading = false
        this.customers = customers;
        console.log(customers);
      },
      (error) => {
        this.loading = false
        console.log(error);
      }
    );
  }

  createCustomer(): void {
    this.router.navigateByUrl('/create')
  }

  customerDetails(row: Customer): void {
    this.router.navigateByUrl(`/customer/${row.id}`)
  }
}
