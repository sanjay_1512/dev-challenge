import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer } from 'src/app/models/Customer';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss'],
})
export class CustomerDetailComponent implements OnInit {
  currentCustomer: Customer | null = null;
  loading = false;

  constructor(
    private customerService: CustomerService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getProduct(this.route.snapshot.paramMap.get('id'));
  }

  getProduct(id: string | null): void {
    if (id) {
      this.loading = true;
      this.customerService.getCustomerById(id).subscribe(
        (customer) => {
          this.loading = false;
          this.currentCustomer = customer;
        },
        (error) => {
          this.loading = false;
          console.log(error);
        }
      );
    }
  }

  backToCustomerList(): void {
    this.router.navigateByUrl(`/customers`)
  }
}
