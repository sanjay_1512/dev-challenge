# README #

This is a  whydonate developer challenge assignment demonstrated with Angular 11+.

### Get Started ###
Clone the repo
git clone https://sanjay_1512@bitbucket.org/sanjay_1512/dev-challenge.git

### Install NPM Packages ###

Install the npm packages described in the package.json and verify that it works:

Run below commands in sequence:

* npm install
* npm run start

The npm run start command builds (compiles TypeScript and copies assets) the application into dist/, watches for changes to the source files, and runs server on port 4200.

Shut it down manually with Ctrl-C.

These are the test-related scripts:

npm test - builds the application and runs Intern tests (both unit and functional) one time.